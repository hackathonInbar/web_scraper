import logging
import pandas as pd
import requests
import config

CB_KEY = '57bc41a6faddc041dcc61cce957ebfde'
BASE_URL = 'https://api.crunchbase.com/v3.1/odm-'
CHUNK_SIZE = 10
headers = {
    'User-Agent': 'My User Agent 1.1',
}


def get_details(api='people'):
    if api == 'people':
        ids, name, table = 'maker_id', 'full_name', 'makers'
        cols = ['first_name', 'last_name', 'gender', 'title', 'organization_permalink',
                'organization_api_path', 'organization_web_path', 'organization_name']
    else:
        ids, name, table = 'product_id', 'title', 'products'
        cols = ["name", "stock_exchange", "stock_symbol", "primary_role", "short_description"]
    columns = [ids, 'permalink', 'api_path', 'web_path', 'api_url', 'profile_image_url',
               'homepage_url', 'facebook_url', 'twitter_url', 'linkedin_url', 'city_name', 'region_name',
               'country_code', 'created_at', 'updated_at'] + cols
    return ids, name, table, columns


def prep_crunchbase_data(input_data, api='people'):
    '''
    prepares data for crunchbase api calls
    api param can be either 'people' or 'organizations'
    '''
    logging.info(f"Now enriching producthunt data with crunchbase {api} API...")
    input_data = tuple(input_data)
    ids, name, table, columns = get_details(api=api)
    df = pd.read_sql(f"select {ids},{name} from {table} where {name} IN {input_data}", config.get_engine(),
                     columns=[ids, name])
    return df, ids, name, columns, api


def add_crunchbase_data_to_db(args):
    '''
    sends requests to the crunchbase api and saves results to db
    '''
    dfs, ids, name, columns, api = args
    url = f"{BASE_URL}{api}?user_key={CB_KEY}"
    enriched_df = pd.DataFrame(columns=columns)
    for index, row in dfs.iterrows():
        querystring = {"name": row[name]}
        response = requests.request("GET", url, params=querystring, headers=headers)
        if response.status_code != 200:
            logging.debug(f"FAILURE:{url} \n {response.status_code}")
        try:
            names_row = pd.DataFrame([response.json()['data']['items'][0]['properties']])
            names_row[ids] = row[ids]
            enriched_df = enriched_df.append(names_row, sort=False)
        except IndexError as e:
            logging.debug(f"{e}: {api} not found in crunchbase")
    enriched_df.reset_index()
    enriched_df.index.names = ['crunchbase_id']
    enriched_df.to_sql(f"crunchbase_{api}", con=config.get_engine(), if_exists='append',
                       index=False,
                       index_label='crunchbase_id', chunksize=CHUNK_SIZE)
    logging.info(f"{enriched_df.shape[0]} {api} were found on crunchbase and added to db")

# def prep_crunchbase_data_from_db(api='people'):
#     '''
#     prepares data for crunchbase api calls
#     api param can be either 'people' or 'organizations'
#     '''
#     logging.info(f"Now enriching producthunt data with crunchbase {api} API...")
#     ids,name,table,columns = get_details(api=api)
#     for col in columns+cols:
#         q = f"ALTER TABLE crunchbase_{api} MODIFY COLUMN {col} VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci"
#         make_tables.execute_query(make_tables.get_con(), q, f"'column {col} charset'")
#
#     existing = tuple(pd.read_sql(f"select {ids} from crunchbase_{api}",config.get_engine(),columns=[id])[id].to_list())
#     if len(existing)>0:
#         df = pd.read_sql(f"select {ids},{name} from {table} where {ids} not IN {existing}",config.get_engine(),
#                          columns=[ids, name],chunksize=CHUNK_SIZE)
#     else:
#         df = pd.read_sql(f"select {ids},{name} from {table}", config.get_engine(),
#                          columns=[ids, name], chunksize=CHUNK_SIZE)
#     return df, ids, name, columns, api

# def add_crunchbase_data_to_db(args,from_db=False):
#     '''
#     sends requests to the crunchbase api and saves results to db
#     '''
#     dfs, id, name, columns, api = args
#     url = f"{BASE_URL}{api}?user_key={CB_KEY}"
#     enriched_df = pd.DataFrame(columns=columns)
#     if from_db:
#         idx = 1
#         for df in dfs:
#             for index, row in df.iterrows():
#                 querystring = {"name": row[name]}
#                 try:
#                     response = requests.request("GET", url, params=querystring,headers=headers)
#                     if response.status_code != 200:
#                         logging.debug(f"FAILURE:{url} \n {response.status_code}")
#                 except Exception as e:
#                     logging.exception(f"{e}")
#                 try:
#                     names_row = pd.DataFrame([response.json()['data']['items'][0]['properties']])
#                     names_row[id] = row[id]
#                     enriched_df = enriched_df.append(names_row, sort=False)
#                 except Exception as e:
#                     logging.debug(f"{e}: {api} not found in crunchbase")
#             logging.info(f"{CHUNK_SIZE * idx} processed")
#             idx += 1
#             try:
#                 enriched_df.to_sql(f"crunchbase_{api}", con=config.get_engine(), if_exists='append', index=False,
#                                    index_label='crunchbase_id', chunksize=CHUNK_SIZE)
#                 logging.info(f"{enriched_df.shape[0]} {api} were found on crunchbase and added to db")
#                 enriched_df = enriched_df.iloc[0:0]
#             except Exception as e:
#                 logging.exception(f"{e}")
#     else:
#         for index, row in dfs.iterrows():
#             querystring = {"name": row[name]}
#             try:
#                 response = requests.request("GET", url, params=querystring,headers=headers)
#                 if response.status_code != 200:
#                     logging.debug(f"FAILURE:{url} \n {response.status_code}")
#             except Exception as e:
#                 logging.exception(f"{e}")
#             try:
#                 names_row = pd.DataFrame([response.json()['data']['items'][0]['properties']])
#                 names_row[id] = row[id]
#                 enriched_df = enriched_df.append(names_row, sort=False)
#             except Exception as e:
#                 logging.debug(f"{e}: {api} not found in crunchbase")
#     enriched_df.reset_index()
#     enriched_df.index.names = ['crunchbase_id']
#     try:
#         enriched_df.to_sql(f"crunchbase_{api}", con=config.get_engine(), if_exists='append',
#                            index=False,
#                            index_label='crunchbase_id', chunksize=CHUNK_SIZE)
#         logging.info(f"{enriched_df.shape[0]} {api} were found on crunchbase and added to db")
#     except Exception as e:
#         logging.exception(f"{e}")
