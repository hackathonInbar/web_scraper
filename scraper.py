import enrich
import ScrapingClasses as SC
import pandas as pd
import logging
import config
import time
import random

# CLI Arguments:
DROP_DB = 0
SCROLL = 1
TOPICS = 2


def get_topics(scrolls=True, write_to_db=True):
    """
    creates TopicsPage instance, scrapes topic names and urls and saves them to db.
    :param scrolls: when true uses selenium to scroll page and get more topics. True gets 100 topics, 9 gets 200.
    :param write_to_db: when false, the function will only return the list of topics without writing to db
    :return: list of topics
    """
    topics_page = SC.TopicsPage(scrolls)
    topics_amount, topics_lst = topics_page.get_topics()
    topics_df = pd.DataFrame(topics_lst, columns=['topic'])
    db_topics = pd.read_sql('select topic from topics', config.get_engine(), columns='topic')
    to_remove = list(set(topics_lst).intersection(set(db_topics['topic'].to_list())))
    topics_df_ = topics_df[~topics_df['topic'].isin(to_remove)]  # remove values that are in the db
    if write_to_db:
        logging.info(
            f"{topics_amount - topics_df_.shape[0]} topics were already in the db, out of {topics_amount} collected.")
        topics_df_.index.names = ['topic_id']
        topics_df_.to_sql('topics', con=config.get_engine(), if_exists='append', index=False, index_label='topic_id')
        logging.info(f"{topics_df_.shape[0]} topics successfully added to the db")
    return topics_lst


def add_products_to_db(topics_lst, scrolls=True, write_to_db=True):
    """
    1. creates TopicPage instance for each topic and gets basic product data.
    2. creates ProductPage instance for each product and gets more data per product by calling enrich_product_data()
    3. saves data on new products to the products table in the db
    """
    topics_lst = topics_lst
    product_columns = ['title', 'subtitle', 'date', 'upvotes', 'reviews', 'review_score', 'comments', 'link', 'e_link',
                       'makers']
    output = pd.DataFrame(columns=product_columns)
    logging.info('Now checking for new products...')
    for i, topic in enumerate(topics_lst):
        logging.info(f"started scraping topic '{topic}'")
        topic_page = SC.TopicPage(topic, scrolls)
        topic_df = topic_page.get_products().replace([r'&#39', r'&amp;'], ['', '&'], regex=True)
        db_links = pd.read_sql('select link from products', config.get_engine(), columns='link')
        to_remove = list(set(db_links.link) & set(topic_df.link))
        topic_df_ = topic_df[~topic_df['link'].isin(to_remove)]
        df = pd.concat([topic_df_, enrich_product_data(topic_df_)], axis=1)  # enrichment
        if write_to_db:
            df = df[product_columns]
            df.index.names = ['product_id']
            try:
                df[['upvotes', 'reviews', 'comments']] = df[['upvotes', 'reviews', 'comments']].apply(pd.to_numeric)
            except ValueError as e:
                logging.exception(f"string found in numbers column {e}")
            df['subtitle'].str.encode(encoding='utf-8')
            # df['subtitle'] = df['subtitle'].apply(lambda x: emoji.emojize(x, use_aliases=True))
            df['subtitle'] = df['subtitle'].apply(lambda x: x.encode('ascii', 'ignore').decode('ascii'))
            df['title'] = df['title'].apply(lambda x: x.encode('ascii', 'ignore').decode('ascii'))
            df.drop(columns=['makers']).to_sql('products', con=config.get_engine(), if_exists='append', index=False,
                                               index_label='product_id')
            logging.info(f"finished scraping topic {i + 1} out of {len(topics_lst)}\n{df.shape[0]} "
                         f"products added to db")
        df["origin_topic"] = topic
        output = output.append(df[product_columns + ['origin_topic']], ignore_index=True, sort=False)
        if i % 50 == 0 and i != 0:
            time.sleep(random.randint(1, 3))
    topics_products_makers_df = output[['link', 'title', 'origin_topic', 'makers']]
    return topics_products_makers_df


def enrich_product_data(df):
    """
    Enriches each topic df with more metrics from the individual product pages.
    """
    if df.shape[0] > 0:
        logging.info(f"fetching data on {df.shape[0]} products..")
    else:
        logging.info('no new products to add')
    enriched_columns = ['date', 'reviews', 'review_score', 'e_link', 'makers']
    enriched_df = pd.DataFrame(columns=enriched_columns)
    for index, row in df.iterrows():
        product_page = SC.ProductPage(row['link'], scrolls=False)
        enriched_row = product_page.enrich_product_data()
        enriched_df.loc[index] = enriched_row
    enriched_df['reviews'] = enriched_df.reviews.str.extract('(\d+)', expand=False).to_list()
    return enriched_df


def add_makers_to_db(makers_lst_of_lst):
    """
    instantiates HTMLPages for maker pages and creates a csv of makers with their details given a list of maker handles.
    """
    makers_lst = [maker for makers in makers_lst_of_lst for maker in makers]
    logging.info(f"accessing makers data...")
    maker_columns = ['handle', 'full_name', 'collections_made', 'followed_collections', 'following', 'followers']
    output = pd.DataFrame(columns=maker_columns)
    db_handles = pd.read_sql('select handle from makers', config.get_engine(), columns=['handle'])
    db_handles = db_handles['handle'].to_list()
    to_remove = list(set(makers_lst).intersection(set(db_handles)))
    logging.info(f"{len(to_remove)} makers were already in the db\nNow adding {len(set(makers_lst)) - len(to_remove)}"
                 f" out of {len(set(makers_lst))} makers collected.")
    for i, maker in enumerate(makers_lst):
        if maker not in db_handles:
            maker_page = SC.MakerPage(maker, scrolls=False)
            df = maker_page.get_makers_data()
            df['handle'] = maker
            output = output.append(df, ignore_index=True, sort=False)
            if i != 0 and i % 50 == 0:
                time.sleep(random.randint(1, 4))
    output = output[maker_columns]
    output = output.drop_duplicates(subset=['handle'])
    output.index.names = ['maker_id']
    output['full_name'] = output['full_name'].apply(lambda x: x.encode('ascii', 'ignore').decode('ascii'))
    output.to_sql('makers', con=config.get_engine(), if_exists='append', index=False, index_label='maker_id')
    logging.info(f"{output.shape[0]} makers added to db")
    return output['full_name']


def add_topics_products_data_to_db(products_topics_df):
    """
    adds product link and topic to the connector table "topics_products"
    """
    try:
        products_topics_df.columns = ['product_link', 'origin_topic']
        products_topics_df = products_topics_df[~products_topics_df.product_link.str.contains('promo')]
        products_topics_df.dropna().to_sql('topics_products', con=config.get_engine(), if_exists='append', index=False)
        logging.info('added topics & products to connector table')
    except Exception as e:
        logging.exception(f"products_topics table error: {e}")


def add_product_maker_data_to_db(products_makers):
    """
    adds product link and maker handle to the connector table "products_makers"
    """
    try:
        products_makers.columns = ['product_link', 'maker_handle']
        products_makers = products_makers.dropna().explode('maker_handle')
        products_makers = products_makers[products_makers.maker_handle != 'None']
        products_makers.to_sql('products_makers', con=config.get_engine(), if_exists='append', index=False)
        logging.info('added products & makers to connector table')
    except Exception as e:
        logging.debug(f"products_makers table error: {e}")
        logging.exception(f"products_makers table error")


def main(*args):
    """
    Scrapes ProductHunt.com website saves data in sql db.
    """
    # CLI arguments:
    drop_db = args[DROP_DB] if args else False
    scroll = args[SCROLL] if args else False
    topics_lst_inpt = args[TOPICS] if args else False
    config.log_config()
    logging.debug(f"drop_db: {drop_db}\ntopics_lst_inpt: {topics_lst_inpt}\nscroll: {scroll}\n.")
    # Database Setup
    import make_tables
    if drop_db:
        make_tables.main(drop=True)
    else:
        make_tables.main()
    logging.info('\n\n***************************\n**scraping flow initiated**\n***************************\n')
    if not topics_lst_inpt:
        topics = get_topics(scrolls=scroll)  # get topics from web
    else:
        get_topics(scrolls=scroll)
        topics = topics_lst_inpt  # use topics from CLI arguments
    df = add_products_to_db(topics, scrolls=scroll)
    makers_lst_of_lst = df['makers'].to_list()
    products_topics = df[['link', 'origin_topic']]
    add_topics_products_data_to_db(products_topics)
    # try:
    enrich.add_crunchbase_data_to_db(enrich.prep_crunchbase_data(df.title, api='organizations'))  # Enrichment
    # except Exception as e:
    #     logging.debug(f"{e}")
    #     logging.info(f"No products to enrich")

    if makers_lst_of_lst:
        products_makers = df[['link', 'makers']]
        new_names = add_makers_to_db(makers_lst_of_lst)
        add_product_maker_data_to_db(products_makers)
        enrich.add_crunchbase_data_to_db(enrich.prep_crunchbase_data(new_names, api='people'))  # Enrichment

    # enrich.add_crunchbase_data_to_db(enrich.prep_crunchbase_data_from_db(api='people'))  # Enrichment only
    # enrich.add_crunchbase_data_to_db(enrich.prep_crunchbase_data_from_db(api='organizations'))  # Enrichment only
    logging.info('\n****************************************\n**scraping flow completed successfully**\n'
                 '****************************************')


if __name__ == '__main__':
    main()
