# README -  Product Hunt Scraper

This incredible program scrapes data from [ProductHunt](https://producthunt.com/). It was built in Shoham and Tel Aviv as part of the [ITC](https://www.itc.tech/) Data Science Fellows Program.

## Data

3 types of data are collected and stored in separate mySQL tables:

```bash
1. Topics
2. Products
3. Makers
```
Data on products and makers is enriched with more information from the [crunchbase open data map API](https://data.crunchbase.com/docs/open-data-map).

## Usage

```bash
pip install -r requirements.txt
```

```bash
python cli.py --scroll --drop_db -t topic_name_1 -t topic_name_2
```
When initiated from the command line, the program prompts the user for a mySQL password to connect to the localhost, create and update a 'product_hunt' database.


CLI Argument | When specified | Not specified
------------ | ------------- | -------------
--scroll | scrolls pages to get 100 items per page | 20 items per page are collected
--drop_db | deletes existing product_hunt db and rebuilds it, replacing any existing content | only adds new records
-t topic_name| does not collect topics and only scrapes specified topics | scrapes all topics that were collected

![GIF](g.gif)

* Running cli.py with no trailing argument will result in saving 20 topics and 400 products, along with their respective makers to the db.

* Adding the --scroll argument will use selenium to get 100 items per page and will result in 100 topic and 10,000 products (and their respective makers).

* The program only adds to the database new entities, unless the --drop_db argument is used in which case the db is rebuilt and the scraped data is saved to a fresh db.

* Specifying topics to scrape is also possible by adding the topic name after the -t argument.


## ERD
![ERD](ERD_enriched.png)
