#!/usr/bin/env python
import logging
import sys
from sqlalchemy import create_engine
import mysql.connector

# DB Credentials:
HOST = 'host'
USER = 'user'
DATABASE = 'db'
# args params
PASS = 0
# logging
LOG_FILE = 'scraper_log.log'


def set_mysql_password(pass_inpt):
    """
    a setter for mysql password which is collected as a prompt in the cli.
    """
    global passw
    passw = pass_inpt


def get_mysql_password():
    """
    a getter for the mySQL password which is collected as a prompt in the cli.
    """
    global passw
    return passw


def get_mysql_cred():
    mysql = {
        'host': 'localhost',
        'user': "newuser",
        'passwd': "password",
        # 'passwd': get_mysql_password(),
        'db': 'product_hunt_cron'
    }
    return mysql


def set_engine():
    """
    a setter for mysql engine.
    defines an sqlalchemy engine to be used as a connector to read and write from the SQL db.
    """
    global engine
    engine = create_engine(f"mysql+mysqlconnector://{get_mysql_cred()[USER]}:{get_mysql_password()}@"
                           f"[{get_mysql_cred()[HOST]}]/{get_mysql_cred()[DATABASE]}", echo=False)


def get_engine():
    """
    a getter for mysql engine.
    defines an sqlalchemy engine to be used as a connector to read and write from the SQL db.
    """
    global engine
    try:
        engine
    except NameError:
        set_engine()
    return engine


def log_config():
    """
    set logging settings.
    """
    logging.basicConfig(filename=LOG_FILE, level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')
    sh = logging.StreamHandler(sys.stdout)
    formatter = logging.Formatter("%(asctime)s;  %(message)s",
                                  "%Y-%m-%d %H:%M:%S")
    sh.setFormatter(formatter)
    logging.getLogger().addHandler(sh)


def main(*args):
    global engine
    try:
        set_mysql_password(args[PASS])
    except mysql.connector.Error:
        logging.exception('no args param')
    set_engine()


if __name__ == '__main__':
    main()
