import logging
import sys
import mysql.connector
import config


def init_credential_params():
    """
    initializing database credentials for connection, based on config file data.
    """
    global host, user, passw, database
    cred_params = config.get_mysql_cred()
    host = cred_params[config.HOST]
    user = cred_params[config.USER]
    database = cred_params[config.DATABASE]
    passw = config.get_mysql_password()


def get_database():
    global database
    yield database


def create_connection(host_name, user_name, user_password, db_name=None):
    connection = None
    try:
        if not db_name:
            connection = mysql.connector.connect(host=host_name, user=user_name, passwd=user_password)
        else:
            connection = mysql.connector.connect(host=host_name, user=user_name, passwd=user_password, database=db_name)
    except mysql.connector.Error:
        logging.exception(f"Unable to connect to mySql. Check credentials on congig.get_mysql_cred()")
        sys.exit()
    return connection


def get_con():
    global host, user, passw, database
    con = create_connection(host, user, passw, database)
    return con


def create_database(connection, query):
    global db_exists

    try:
        cursor = connection.cursor()
        cursor.execute(query)
        logging.info("Database created successfully")
        db_exists = False
    except (mysql.connector.Error,AttributeError) as e:
        db_exists = True
        logging.info(f"Database exists")
        logging.debug(f"The error '{e}' occurred")


def execute_query(connection, query, query_name):
    cursor = connection.cursor()
    try:
        cursor.execute(query)
        connection.commit()
        logging.info(f"Query {query_name} executed successfully")
    except mysql.connector.Error as e:
        logging.info(f"db was not dropped because it didn't exist")
        logging.debug(f"The error '{e}' occurred")


create_database_query = "CREATE DATABASE " + 'product_hunt'

create_topics_table = '''
CREATE table IF NOT EXISTS topics(topic_id INTEGER PRIMARY KEY AUTO_INCREMENT, topic VARCHAR(200) NOT NULL)'''

create_topic_index = '''CREATE UNIQUE INDEX idx_topic ON topics (topic)'''

create_products_table = ''' 
CREATE table IF NOT EXISTS products(
product_id INTEGER PRIMARY KEY AUTO_INCREMENT,
   title VARCHAR(200) NOT NULL,
   subtitle VARCHAR(200),
   date VARCHAR(200),
   upvotes INT,
   reviews INT,
   review_score DOUBLE,
   comments INT,
   link VARCHAR(200),
   e_link VARCHAR(200)
   )
'''

create_link_index = '''CREATE UNIQUE INDEX idx_link ON products (link)'''

create_makers_table = '''
CREATE table IF NOT EXISTS makers(
maker_id INTEGER PRIMARY KEY AUTO_INCREMENT,
   handle VARCHAR(200) NOT NULL,
   full_name VARCHAR(200),
   collections_made INT,
   followed_collections INT,
   following INT,
   followers INT  
   )
'''

create_handle_index = '''CREATE UNIQUE INDEX idx_handle ON makers (handle)'''

create_products_makers_table = '''
CREATE table IF NOT EXISTS products_makers(
product_link VARCHAR(200),
maker_handle VARCHAR(200),
FOREIGN KEY (product_link) REFERENCES products (link), 
FOREIGN KEY (maker_handle) REFERENCES makers (handle) 
)
'''

create_topics_products_table = '''
CREATE table IF NOT EXISTS topics_products(
product_link VARCHAR(200),
origin_topic VARCHAR(200),
FOREIGN KEY (product_link) REFERENCES products (link), 
FOREIGN KEY (origin_topic) REFERENCES topics (topic) 
)
'''

create_crunchbase_names_table = '''
CREATE table IF NOT EXISTS crunchbase_people(
crunchbase_id INTEGER PRIMARY KEY AUTO_INCREMENT,
maker_id INTEGER,
permalink VARCHAR(200), 
api_path VARCHAR(200), 
web_path VARCHAR(200), 
api_url VARCHAR(200), 
first_name VARCHAR(200), 
last_name VARCHAR(200), 
gender VARCHAR(200), 
title VARCHAR(200), 
organization_permalink VARCHAR(200),
organization_api_path VARCHAR(200), 
organization_web_path VARCHAR(200), 
organization_name VARCHAR(200), 
profile_image_url VARCHAR(200), 
homepage_url VARCHAR(200),
facebook_url VARCHAR(200), 
twitter_url VARCHAR(200), 
linkedin_url VARCHAR(200), 
city_name VARCHAR(200), 
region_name VARCHAR(200), 
country_code VARCHAR(200), 
created_at VARCHAR(200), 
updated_at VARCHAR(200),
FOREIGN KEY (maker_id) REFERENCES makers (maker_id)
)
'''

create_crunchbase_orgs_table = ''' 
CREATE table IF NOT EXISTS crunchbase_organizations(
crunchbase_id INTEGER PRIMARY KEY AUTO_INCREMENT,
product_id INTEGER,
permalink VARCHAR(200), 
api_path VARCHAR(200), 
web_path VARCHAR(200), 
api_url VARCHAR(200), 
name VARCHAR(200), 
stock_exchange VARCHAR(200), 
stock_symbol VARCHAR(200), 
primary_role VARCHAR(200), 
short_description VARCHAR(200),
profile_image_url VARCHAR(200), 
domain VARCHAR(200),  
homepage_url VARCHAR(200),
facebook_url VARCHAR(200), 
twitter_url VARCHAR(200), 
linkedin_url VARCHAR(200), 
city_name VARCHAR(200), 
region_name VARCHAR(200), 
country_code VARCHAR(200), 
created_at VARCHAR(200), 
updated_at VARCHAR(200),
FOREIGN KEY (product_id) REFERENCES products (product_id)
)
'''


def main(drop=False):
    """
    Establishes connection to mySQL, creates database
    :param drop: if true it drops the DB an recreates it
    """
    global host, user, passw
    init_credential_params()
    conn = create_connection(host, user, passw)

    if drop:
        execute_query(conn, f"DROP DATABASE {config.get_mysql_cred()['db']}", 'Drop DB')

    create_database(conn, create_database_query)

    if not db_exists:
        execute_query(get_con(), create_topics_table, 'create_topics_table')
        execute_query(get_con(), create_products_table, 'create_products_table')
        execute_query(get_con(), create_makers_table, 'create_makers_table')

        execute_query(get_con(), create_topic_index, 'create_topic_index')
        execute_query(get_con(), create_link_index, 'create_link_index')
        execute_query(get_con(), create_handle_index, 'create_handle_index')

        execute_query(get_con(), create_topics_products_table, 'create_topics_products_table')
        execute_query(get_con(), create_products_makers_table, 'create_products_makers_table')
        execute_query(get_con(), create_crunchbase_names_table, 'create_crunchbase_names_table')
        execute_query(get_con(), create_crunchbase_orgs_table, 'create_crunchbase_orgs_table')


if __name__ == '__main__':
    main()
