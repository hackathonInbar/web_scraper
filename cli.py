import click
import ast
import config
import scraper

PRODUCTS_TABLE = 'products'
MAKERS_TABLE = 'makers'
TOPICS_TABLE = 'topics'


class PythonLiteralOption(click.Option):
    def type_cast_value(self, ctx, value):
        try:
            return ast.literal_eval(value)
        except:
            raise click.BadParameter(value)

@click.command()
@click.option('--drop_db', is_flag=True, default=False, help='Optional: if specified, drops db and recreates'
                                                             'the DB, before scraping new data.')
@click.option('--scroll', is_flag=True, default=False, help='Optional: whether or not to scroll down the web-page while'
                                                            'scraping. True if specified, False otherwise.')
@click.option('--topics', '-t', multiple=True, help='Optional: topic names you want to get. If not specified,'
                                                    'scrape topics from topics page.')
def fetch(drop_db, scroll, topics):
    click.echo("Welcome to ProductHunt web scraper\n"
               "If config.py is not altered, the data will be saved locally in your localhost mysql server with the user 'root'.\n")
    # passw = click.prompt('Please insert your mysql password', hide_input=True, confirmation_prompt=True)
    passw = "password"
    if drop_db:
        click.echo("\n- You've decided to drop the DB, therefore we'll recreate the DB, before scraping new data.")
    if scroll:
        click.echo("\n- You've decided to use the option of scrolling down the web pages.")
    else:
        click.echo("\n- You've decided --not-- to use the option of scrolling down the web pages.")
    click.echo("\nWe're fetching the data you asked for.")
    if len(topics):
        click.echo("\nThe following topics would be fetched:\n")
        click.echo('\n'.join(topics))
    else:
        click.echo("\nTopics from 'Topics' page would be fetched.")
    click.echo('\n')
    topics_lst = list(topics)
    config.main(passw)
    scraper.main(drop_db, scroll, topics_lst)


def main():
    fetch()


if __name__ == '__main__':
    main()
