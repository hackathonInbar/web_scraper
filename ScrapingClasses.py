import logging
import sys
import time

import pandas as pd
import requests
from bs4 import BeautifulSoup
from selenium import webdriver

# from pyvirtualdisplay import Display
# from selenium.webdriver import FirefoxOptions
# from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
# from selenium.webdriver.firefox.options import Options

# requirement: Firefox driver for Selenium

URL = 'https://www.producthunt.com'


def scroll(driver, timeout=3, count=4):
    """
    scrolls pages using selenium.
    4 scrolls (default) gets up to 100 items per page.
    """
    scroll_pause_time = timeout
    last_height = driver.execute_script("return document.body.scrollHeight")
    if count is True:
        count = 30
    for i in range(count):
        driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        time.sleep(scroll_pause_time)
        new_height = driver.execute_script("return document.body.scrollHeight")
        if new_height == last_height:
            break
        last_height = new_height


class ProductHuntPage:
    def __init__(self, url, do_scroll):
        """
        HTMLPage constructor
        """
        self.url = url
        self.page = self.get_page_content(do_scroll)

    def get_page_content(self, do_scroll):
        """
        returns the content of the html page in a readable format
        """
        if do_scroll:
            # cap = DesiredCapabilities.FIREFOX
            # cap['marionette'] = True
            # options = FirefoxOptions()
            # options.headless = True
            # display = Display(visible=0, size=(1024, 768))
            # display.start()
            # driver = webdriver.Firefox(options=options,capabilities=cap)
            driver = webdriver.Firefox()
            driver.get(self.url)
            scroll(driver, count=do_scroll)
            page_content = BeautifulSoup(driver.page_source, 'lxml')
            driver.close()
            # display.stop()
            return page_content
        else:
            try:
                page_source = requests.get(self.url).text
                page_content = BeautifulSoup(page_source, 'lxml')
                return page_content
            except ConnectionError as e:  # todo: handle different expections
                logging.exception(f"{e}\nERROR:\nConnection issue\nProgram interrupted")
                sys.exit(0)
            except Exception as e:
                logging.exception(f"error with {self.url} {str(e)}")

    def get_page_fields(self, tag_name, class_name, is_link=False, maker_lst=False, maker_stat=False):
        """
        returns list of values for each field given html parameters
        """
        params = [str(param) for param in self.page.find_all(tag_name, class_name)]
        param_lst = []
        for param in params:
            if is_link:
                try:
                    param_lst.append(param.split('"')[3])
                except IndexError:
                    param_lst.append(float(param.split('>')[1].split('<')[0]))
            elif maker_lst:
                param_lst.append(param.split("/")[1].split('"')[0])
            elif maker_stat:
                param_lst.append(param.split('>')[2].split('<')[0])
            else:
                param_lst.append(param.split(">")[-2].split("<")[0])
        return param_lst


class TopicsPage(ProductHuntPage):

    def __init__(self, do_scroll):
        """
        TopicsPage constructor
        """
        url = URL + "/topics"
        ProductHuntPage.__init__(self, url, do_scroll)

    def get_topics(self):
        """
        returns a list of topics from the /topics page.
        """
        raw_topics = self.page.find_all('a')
        topics = []
        for topic in raw_topics:
            topics.append(topic.get('href'))
        topics = list(set([t.split("/")[2] for t in topics if "topics/" in t]))
        return len(topics), topics


class TopicPage(ProductHuntPage):

    def __init__(self, topic, do_scroll):
        """
        ProductPage constructor
        """
        url = f"{URL}/topics/{topic}"
        ProductHuntPage.__init__(self, url, do_scroll)

    def get_products(self):
        """
        return a DataFrame including the top 20 daily products for a given topic.
        The dataframe includes four main data points: product title, subtitle, upvotes and main topic
        """
        product_names = self.get_page_fields("h3",
                                             "font_9d927 medium_51d18 semiBold_e201b title_9ddaf lineHeight_042f1 "
                                             "underline_57d3c")
        product_descriptions = self.get_page_fields("p",
                                                    "font_9d927 grey_bbe43 small_231df normal_d2e66 tagline_619b7 "
                                                    "lineHeight_042f1 underline_57d3c")
        product_upvotes = self.get_page_fields("span",
                                               "font_9d927 small_231df semiBold_e201b lineHeight_042f1 underline_57d3c")
        product_main_topic = self.get_page_fields("span",
                                                  "font_9d927 grey_bbe43 xSmall_1a46e lineHeight_042f1 underline_57d3c")
        product_comments = [x.replace(u'\xa0', u' ') for x in
                            self.get_page_fields("span", "font_9d927 xSmall_1a46e semiBold_e201b button"
                                                         "Container_b6eb3 lineHeight_042f1 underline_57d3c"
                                                         " uppercase_a49b4")[5::1] if x]
        product_page_link = self.get_page_fields("a", "link_523b9", is_link=True)
        product_main_topic = [i.lower().replace(" ", "-") for i in product_main_topic]
        df = pd.DataFrame(list(zip(product_names, product_descriptions, product_upvotes,
                                   product_comments, product_main_topic, product_page_link)),
                          columns=['title', 'subtitle', 'upvotes', 'comments', 'main_topic', 'link'])
        return df


class ProductPage(ProductHuntPage):
    def __init__(self, product, scrolls):
        """
        ProductPage constructor
        """
        url = URL + product
        ProductHuntPage.__init__(self, url, scrolls)

    def enrich_product_data(self):
        """
        enriches each product with more information from its product page
        """
        if "posts" in self.url:  # this is to handle promotional pages that don't have data

            date = self.get_page_fields("span",
                                        "font_9d927 grey_bbe43 xSmall_1a46e normal_d2e66 timestamp_38f9d "
                                        "lineHeight_042f1 underline_57d3c uppercase_a49b4",
                                        is_link=True)
            review_count = self.get_page_fields("span",
                                                "font_9d927 grey_bbe43 small_231df normal_d2e66 lineHeight_042f1 "
                                                "underline_57d3c")
            review_s = self.get_page_fields("span",
                                            "font_9d927 small_231df semiBold_e201b rating_9e6bb lineHeight_042f1 "
                                            "underline_57d3c",
                                            is_link=True)
            maker_lst = self.get_page_fields("a",
                                             "card_db523",
                                             maker_lst=True)
            external_link = self.get_page_fields('span',
                                                 'font_9d927 grey_bbe43 small_231df normal_d2e66 description_4c630 '
                                                 'lineHeight_042f1 underline_57d3c')
            enriched_row = [date, review_count, review_s, external_link]
            enriched_row = [i[0] if len(i) > 0 else None for i in enriched_row]
            enriched_row.append(maker_lst)
        else:
            enriched_row = [None, None, None, 'None', ['None']]

        return enriched_row


class MakerPage(ProductHuntPage):
    def __init__(self, handle, scrolls):
        """
        MakerPage constructor
        """

        url = URL + "/" + handle + "/made"
        ProductHuntPage.__init__(self, url, scrolls)

    def get_makers_data(self):
        """
        Returns a DataFrame with maker data.
        """

        full_name = self.get_page_fields("div",
                                         "font_9d927 white_ce488 xLarge_18016 semiBold_e201b xSmall_7c9bc userNameContainer_a9a8f lineHeight_042f1 underline_57d3c",
                                         maker_stat=True)
        maker_stats = self.get_page_fields("a",
                                           "font_9d927 black_476ed small_231df normal_d2e66 lineHeight_042f1 underline_57d3c",
                                           maker_stat=True)
        products_lst = self.get_page_fields("a", "link_523b9", is_link=True)
        f = full_name
        f.extend([products_lst])
        a = [f]
        a[0].extend([int(i.replace(',', '')) for i in maker_stats[-4:]])  # add maker_stats and cast strings to ints
        try:
            a[0].append(len((a[0][1])))  # add product_count column
            df = pd.DataFrame(a,
                              columns=["full_name", 'products', 'collections_made', 'followed_collections', 'following',
                                       'followers', 'product_count'])
            return df
        except IndexError:
            return pd.DataFrame()
